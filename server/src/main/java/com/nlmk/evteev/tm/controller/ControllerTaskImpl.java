package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.controller.interfaces.IControllerTask;
import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.service.UserService;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;

import java.io.IOException;

@NoArgsConstructor
@WebService(endpointInterface = "com.nlmk.evteev.tm.controller.interfaces.IControllerTask")
public class ControllerTaskImpl implements IControllerTask {

    private final TaskService taskService = TaskService.getInstance();
    private final UserService userService = UserService.getInstance();
    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();


    @Override
    public ResponseDTO create(final Task task) {
        if (userService.getCurrentUser() != null) {
            task.setUserId(userService.getCurrentUser().getId());
        }
        return taskService.create(task);
    }

    @Override
    public ResponseDTO updateByIndex(final int index, Task task) throws TaskNotFoundException {
        return taskService.updateByIndex(index, task);
    }

    @Override
    public ResponseDTO updateById(final long id, Task task) throws TaskNotFoundException {
        return taskService.updateById(id, task);
    }

    @Override
    public ResponseDTO clear() throws TaskNotFoundException {
        if (userService.getCurrentUser() == null) {
            taskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.getCurrentUser().getId()).getPayloadTask()) {
                taskService.removeById(task.getId());
            }
        }
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO removeByName(final String name) throws TaskNotFoundException {
        taskService.removeByName(name);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO removeById(final long id) throws TaskNotFoundException {
        return taskService.removeById(id);
    }

    @Override
    public ResponseDTO removeByIndex(final int index) throws TaskNotFoundException {
        return taskService.removeByIndex(index);
    }

    @Override
    public ResponseDTO viewById(final Long id) throws TaskNotFoundException {
        return taskService.findById(id);
    }

    @Override
    public ResponseDTO viewByIndex(final int index) throws TaskNotFoundException {
        return taskService.findByIndex(index);
    }

    @Override
    public ListResponseDTO viewByName(final String name) throws TaskNotFoundException {
        return taskService.findByName(name);
    }

    @Override
    public ListResponseDTO list() {
        ListResponseDTO projectList;
        if (userService.getCurrentUser() == null) {
            projectList = taskService.findAll();
        } else {
            projectList = taskService.findAllByUserId(userService.getCurrentUser().getId());
        }
        return projectList;
    }

    @Override
    public ListResponseDTO listTaskByProjectId(final long projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    public ResponseDTO addTaskToProjectByIds(final long projectId, final long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        return projectTaskService.addTaskToProject(projectId, taskId);
    }

    @Override
    public ResponseDTO removeTaskFromProjectByIds(final long projectId, final long taskId) {
        return projectTaskService.removeTaskFromProject(projectId, taskId);
    }

    @Override
    public ResponseDTO saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.saveJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.saveXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.uploadFromJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        taskService.uploadFromXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

}

