package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;

public interface ITaskIService extends IService<Task> {
    ListResponseDTO findAllByProjectId(final Long projectId);

    ResponseDTO findByProjectIdAndId(final Long projectId, final Long id);

    ListResponseDTO findAllByUserId(Long Id);

    ListResponseDTO findByName(final String name) throws TaskNotFoundException;

    ListResponseDTO removeByName(final String name) throws TaskNotFoundException;
}
