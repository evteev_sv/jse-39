package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.controller.ControllerProjectImpl;
import com.nlmk.evteev.tm.controller.ControllerTaskImpl;
import com.nlmk.evteev.tm.controller.ControllerUserImpl;
import jakarta.xml.ws.Endpoint;

public class Application {

    public static void main(final String[] args) {
        ControllerProjectImpl projectController = new ControllerProjectImpl();
        Endpoint.publish("http://localhost:8899/ws/project", projectController);

        ControllerTaskImpl taskController = new ControllerTaskImpl();
        Endpoint.publish("http://localhost:8899/ws/task", taskController);

        ControllerUserImpl userController = new ControllerUserImpl();
        Endpoint.publish("http://localhost:8899/ws/user", userController);

    }

}
