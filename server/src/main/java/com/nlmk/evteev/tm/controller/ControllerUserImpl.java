package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.controller.interfaces.IControllerUser;
import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import com.nlmk.evteev.tm.service.UserService;
import jakarta.jws.WebService;

import java.io.IOException;
import java.util.Optional;

@WebService(endpointInterface = "com.nlmk.evteev.tm.controller.interfaces.IControllerUser")
public class ControllerUserImpl implements IControllerUser {

    private final UserService userService = UserService.getInstance();

    @Override
    public ResponseDTO create(final User user) {
        return userService.create(user);
    }

    @Override
    public ResponseDTO updateByIndex(final int index, final User user) throws UserNotFoundException {
        return userService.updateByIndex(index, user);
    }

    @Override
    public ResponseDTO updateById(final long id, final User user) throws UserNotFoundException {
        return userService.updateById(id, user);
    }

    @Override
    public ResponseDTO updateByName(final String name, final User user) throws UserNotFoundException {
        return userService.updateByName(name, user);
    }

    @Override
    public ResponseDTO updatePasswordByLogin(final String name, String password) throws UserNotFoundException {
        return userService.updatePasswordByLogin(name, password);
    }

    @Override
    public ResponseDTO viewById(final Long id) throws UserNotFoundException {
        return userService.findById(id);
    }

    @Override
    public ResponseDTO viewByIndex(final int index) throws UserNotFoundException {
        return userService.findByIndex(index);
    }

    @Override
    public ResponseDTO viewByName(final String name) throws UserNotFoundException {
        return userService.findByName(name);
    }

    @Override
    public ResponseDTO clear() {
        userService.clear();
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO removeByName(final String name) throws UserNotFoundException {
        return userService.removeByName(name);
    }

    @Override
    public ResponseDTO removeByIndex(final int index) throws UserNotFoundException {
        return userService.removeByIndex(index);
    }

    @Override
    public ResponseDTO removeById(final long id) throws UserNotFoundException {
        return userService.removeById(id);
    }

    @Override
    public ListResponseDTO list() {
        return userService.findAll();
    }

    @Override
    public ResponseDTO displayUserInfo() throws UserNotFoundException {
        if (userService.getCurrentUser() != null) {
            return viewById(userService.getCurrentUser().getId());
        }
        throw new UserNotFoundException("Registry.");
    }

    @Override
    public ResponseDTO registry(final String name, String password) throws UserNotFoundException {
        final var user = userService.findByName(name);
        if (user.getStatus() == Status.DB_ERROR) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (userService.checkPassword(Optional.of(user.getPayloadUser()), password)) {
            userService.setCurrentUser(user.getPayloadUser());
            return ResponseDTO.builder().status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO logOff() {
        userService.setCurrentUser(null);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO updatePassword(String oldPassword, String newPassword) throws UserNotFoundException {
        if (userService.getCurrentUser() == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        } else {
            if (userService.checkPassword(Optional.of(userService.getCurrentUser()), oldPassword)) {
                userService.updatePasswordByLogin(userService.getCurrentUser().getName(), newPassword);
                return ResponseDTO.builder().status(Status.OK).build();
            } else {
                return ResponseDTO.builder().status(Status.DB_ERROR).build();
            }
        }
    }

    @Override
    public ResponseDTO saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.saveJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.saveXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.uploadFromJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        userService.uploadFromXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }
}
