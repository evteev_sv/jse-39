package com.nlmk.evteev.tm.entity;

import com.nlmk.evteev.tm.enumerated.Role;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
public class User implements Serializable {

    public static final Long serialVersionUID = 1L;

    private Long id = System.nanoTime();

    private String name = "";

    private String password;

    private String firstName = "";

    private String lastName = "";

    private Role role = Role.USER;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(Long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, String firstName, String lastName, Role role) {
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public User(Long id, String name, String password, String firstName, String lastName, Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

}
