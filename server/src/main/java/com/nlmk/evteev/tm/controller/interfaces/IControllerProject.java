package com.nlmk.evteev.tm.controller.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IControllerProject extends IController {

    @WebMethod
    ResponseDTO create(final Project project);

    @WebMethod
    ResponseDTO updateByIndex(@WebParam(name = "index") final int index, final Project project) throws ProjectNotFoundException;

    @WebMethod
    ResponseDTO updateById(@WebParam(name = "id") final long id, final Project project) throws ProjectNotFoundException;

    @WebMethod
    ListResponseDTO viewByName(@WebParam(name = "name") final String name) throws ProjectNotFoundException;

}
