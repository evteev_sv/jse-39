package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.interfaces.ITaskIService;
import com.nlmk.evteev.tm.util.Helper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


public class TaskService implements ITaskIService {

    private static TaskService instance = null;
    private final TaskRepository taskRepository;
    private final UserService userService = UserService.getInstance();

    private TaskService() {
        this.taskRepository = TaskRepository.getInstance();
    }

    public static TaskService getInstance() {
        if (instance == null) {
            synchronized (TaskService.class) {
                if (instance == null) {
                    instance = new TaskService();
                }
            }
        }
        return instance;
    }

    @Override
    public ResponseDTO create(final Task task) {
        String name = task.getName();
        if (name.equals("") || Helper.checkProjectName(name) || name.isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<Task> taskOptional = taskRepository.create(task);
        if (taskOptional.isPresent()) {
            return ResponseDTO.builder().payloadTask(taskOptional.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateByIndex(final int index, Task task) throws TaskNotFoundException {
        Optional<Task> task1 = taskRepository.findByIndex(index - 1);
        if (task1.isPresent()) {
            Task updatedTask = Task.builder()
                    .id(task1.get().getId())
                    .name(task.getName())
                    .description(task.getDescription())
                    .userId(task.getUserId()).build();
            task1 = taskRepository.update(updatedTask);
            if (task1.isPresent()) {
                return ResponseDTO.builder().payloadTask(task1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateById(final long id, Task task) throws TaskNotFoundException {
        Optional<Task> task1 = taskRepository.findById(id);
        if (task1.isPresent()) {
            Task updatedTask = Task.builder()
                    .id(task1.get().getId())
                    .name(task.getName())
                    .description(task.getDescription())
                    .userId(task.getUserId()).build();
            task1 = taskRepository.update(updatedTask);
            if (task1.isPresent()) {
                return ResponseDTO.builder().payloadTask(task1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public ResponseDTO findByIndex(final int index) throws TaskNotFoundException {
        Optional<Task> task = taskRepository.findByIndex(index - 1);
        if (task.isPresent()) {
            return ResponseDTO.builder().payloadTask(task.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO findByName(final String name) throws TaskNotFoundException {
        List<Task> task1 = taskRepository.findByName(name);
        if (task1.isEmpty()) {
            return ListResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListResponseDTO
                .builder()
                .payloadTask(task1.toArray(new Task[0]))
                .status(Status.OK).build();
    }

    @Override
    public ResponseDTO findById(final Long id) throws TaskNotFoundException {
        Optional<Task> task1 = taskRepository.findById(id);
        if (task1.isPresent()) {
            return ResponseDTO.builder().payloadTask(task1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeByIndex(final Integer index) throws TaskNotFoundException {
        Optional<Task> task1 = taskRepository.removeByIndex(index);
        if (task1.isPresent()) {
            return ResponseDTO.builder().payloadTask(task1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeById(final Long id) throws TaskNotFoundException {
        Optional<Task> task1 = taskRepository.removeById(id);
        if (task1.isPresent()) {
            return ResponseDTO.builder().payloadTask(task1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO removeByName(final String name) throws TaskNotFoundException {
        List<Task> task1 = taskRepository.removeByName(name);
        return ListResponseDTO
                .builder()
                .payloadTask(task1.toArray(new Task[0]))
                .status(Status.OK).build();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public ListResponseDTO findAll() {
        return ListResponseDTO
                .builder()
                .payloadTask(taskRepository.findAll().get().toArray(new Task[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListResponseDTO findAllByProjectId(final Long projectId) {
        if (projectId == null) return ListResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListResponseDTO
                .builder()
                .payloadTask(taskRepository.findAllByProjectId(projectId).get().toArray(new Task[0]))
                .status(Status.OK).build();
    }

    @Override
    public ResponseDTO findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        if (id == null) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, id);
        if (task.isPresent()) {
            return ResponseDTO
                    .builder()
                    .payloadTask(task.get())
                    .status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO findAllByUserId(final Long userId) {
        if (userId == null) return ListResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListResponseDTO
                .builder()
                .payloadTask(taskRepository.findAllByUserId(userId).get().toArray(new Task[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListResponseDTO saveJSON(final String fileName) throws IOException {
        int status = taskRepository.saveJSON(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO saveXML(final String fileName) throws IOException {
        int status = taskRepository.saveXML(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromJSON(final String fileName) throws IOException {
        int status = taskRepository.uploadJSON(fileName, Task.class);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromXML(final String fileName) throws IOException {
        int status = taskRepository.uploadXML(fileName, Task.class);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

}
