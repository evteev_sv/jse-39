package com.nlmk.evteev.tm.util;

import java.util.List;
import java.util.regex.Pattern;

public class Helper {

    public static String getNameFromParameters(List<String> parameters) {
        if (parameters.size() > 0) {
            return parameters.get(0);
        } else {
            return "";
        }
    }

    public static int inputIndexCheckFormat(String input) {
        final int index;
        try {
            index = Integer.parseInt(input) - 1;
        } catch (Throwable t) {
            return -1;
        }
        return index;
    }

    public static long inputIdCheckFormat(String command) {
        final long id;
        try {
            id = Long.parseLong(command);
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

    public static boolean checkProjectName(String name) {
        if (Pattern.compile("^\\d").matcher(name).lookingAt()) {
            System.out.println("[NAMES SHOULD NOT START FROM NUMBERS]");
            return true;
        }
        return false;
    }
}
