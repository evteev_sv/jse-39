package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.controller.interfaces.IControllerProject;
import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import com.nlmk.evteev.tm.service.UserService;
import jakarta.jws.WebService;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Arrays;

@Log4j2
@WebService(endpointInterface = "com.nlmk.evteev.tm.controller.interfaces.IControllerProject")
public class ControllerProjectImpl implements IControllerProject {

    private final ProjectService projectService;
    private final UserService userService;
    private final ProjectTaskService projectTaskService;

    public ControllerProjectImpl() {
        this.projectService = ProjectService.getInstance();
        this.userService = UserService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
    }

    @Override
    public ResponseDTO create(final Project project) {
        if (userService.getCurrentUser() != null) {
            project.setUserId(userService.getCurrentUser().getId());
        }
        return projectService.create(project);
    }

    @Override
    public ResponseDTO updateByIndex(final int index, Project project) throws ProjectNotFoundException {
        return projectService.updateByIndex(index, project);
    }

    @Override
    public ResponseDTO updateById(final long id, Project project) throws ProjectNotFoundException {
        return projectService.updateById(id, project);
    }

    @Override
    public ResponseDTO clear() throws ProjectNotFoundException {
        if (userService.getCurrentUser() == null) {
            projectService.clear();
            projectTaskService.clear();
        } else {
            for (Project project : projectService.findAllByUserId(userService.getCurrentUser().getId()).getPayloadProject()) {
                projectService.removeById(project.getId());
            }
        }
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO removeByName(final String name) throws ProjectNotFoundException {
        final var projects = projectService.removeByName(name);
        Arrays.stream(projects.getPayloadProject()).forEach(project -> {
            final var tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks.getPayloadTask()) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
        });
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO removeById(final long id) throws ProjectNotFoundException {
        return projectService.removeById(id);
    }

    @Override
    public ResponseDTO removeByIndex(final int index) throws ProjectNotFoundException {
        return projectService.removeByIndex(index);
    }

    @Override
    public ResponseDTO viewById(final Long id) throws ProjectNotFoundException {
        return projectService.findById(id);
    }

    @Override
    public ResponseDTO viewByIndex(final int index) throws ProjectNotFoundException {
        return projectService.findByIndex(index);
    }

    @Override
    public ListResponseDTO viewByName(final String name) throws ProjectNotFoundException {
        return projectService.findByName(name);
    }

    @Override
    public ListResponseDTO list() {
        ListResponseDTO projectList;
        if (userService.getCurrentUser() == null) {
            projectList = projectService.findAll();
        } else {
            projectList = projectService.findAllByUserId(userService.getCurrentUser().getId());
        }
        return projectList;

    }

    @Override
    public ResponseDTO saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.saveJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.saveXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.uploadFromJSON(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ResponseDTO uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        projectService.uploadFromXML(fileName);
        return ResponseDTO.builder().status(Status.OK).build();
    }
}
