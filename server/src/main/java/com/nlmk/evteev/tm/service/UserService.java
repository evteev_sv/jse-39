package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.interfaces.IUserIService;

import java.io.IOException;
import java.util.Deque;
import java.util.Optional;

import static com.nlmk.evteev.tm.util.HashUtil.hashMD5;

public class UserService implements IUserIService {

    private static UserService instance = null;
    private final UserRepository userRepository;

    private UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    public static UserService getInstance() {
        if (instance == null) {
            synchronized (UserService.class) {
                if (instance == null) {
                    instance = new UserService();
                }
            }
        }
        return instance;
    }

    @Override
    public ResponseDTO create(final User user) {
        if (user.getName() == null || user.getName().isEmpty() || user.getPassword() == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (user.getRole() == null) {
            user.setRole(Role.USER);
        }
        User user1 = User.builder()
                .id(user.getId())
                .name(user.getName())
                .password(hashMD5(user.getPassword()))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole()).build();
        Optional<User> userOptional = userRepository.create(user1);
        if (userOptional.isPresent()) {
            return ResponseDTO.builder().payloadUser(userOptional.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateByIndex(final int index, final User user) throws UserNotFoundException {
        if (user.getName() == null || user.getName().isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (user.getPassword() == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user1 = userRepository.findByIndex(index - 1);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(user.getName())
                    .password(hashMD5(user.getPassword()))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return ResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateById(final long id, final User user) throws UserNotFoundException {
        if (user.getName() == null || user.getName().isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (user.getPassword() == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user1 = userRepository.findById(id);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(user.getName())
                    .password(hashMD5(user.getPassword()))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return ResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateByName(final String name, final User user) throws UserNotFoundException {
        if (user.getName() == null || user.getName().isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (user.getPassword() == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user1 = userRepository.findByName(name);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(user.getName())
                    .password(hashMD5(user.getPassword()))
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return ResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updatePasswordByLogin(String login, String password) throws UserNotFoundException {
        if (login == null || login.isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (password == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        User user = User.builder()
                .name(login)
                .password(hashMD5(password))
                .build();
        Optional<User> user1 = userRepository.updatePassword(user);
        if (user1.isPresent()) {
            return ResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updatePasswordById(Long id, String password) throws UserNotFoundException {
        if (id == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        if (password == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        User user = User.builder()
                .id(id)
                .password(hashMD5(password))
                .build();
        Optional<User> user1 = userRepository.updatePassword(user);
        if (user1.isPresent()) {
            return ResponseDTO.builder().payloadUser(user1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public boolean checkPassword(final Optional<User> user, final String password) {
        return user.map(value -> value.getPassword().equals(hashMD5(password))).orElse(false);
    }

    @Override
    public ResponseDTO findByName(String login) throws UserNotFoundException {
        if (login == null || login.isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user = userRepository.findByName(login);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO findById(Long id) throws UserNotFoundException {
        if (id == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO findByIndex(final int index) throws UserNotFoundException {
        Optional<User> user = userRepository.findByIndex(index - 1);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeByName(String login) throws UserNotFoundException {
        if (login == null || login.isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user = userRepository.removeByName(login);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeById(Long id) throws UserNotFoundException {
        if (id == null) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> user = userRepository.removeById(id);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeByIndex(Integer index) throws UserNotFoundException {
        Optional<User> user = userRepository.removeByIndex(index);
        if (user.isPresent()) {
            return ResponseDTO.builder().payloadUser(user.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public ListResponseDTO findAll() {
        return ListResponseDTO
                .builder()
                .payloadUser(userRepository.findAll().get().toArray(new User[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListResponseDTO saveJSON(final String fileName) throws IOException {
        int status = userRepository.saveJSON(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO saveXML(final String fileName) throws IOException {
        int status = userRepository.saveXML(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromJSON(final String fileName) throws IOException {
        int status = userRepository.uploadJSON(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromXML(final String fileName) throws IOException {
        int status = userRepository.uploadXML(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public User getCurrentUser() {
        return userRepository.currentUser;
    }

    @Override
    public void setCurrentUser(User user) {
        userRepository.currentUser = user;
    }

    @Override
    public Deque<String> getHistory() {
        return userRepository.history;
    }

    @Override
    public int addCommandToHistory(String command) {
        return userRepository.addCommandToHistory(command);
    }

    @Override
    public int getHistoryLimit() {
        return userRepository.historyLimit;
    }

    @Override
    public void setHistoryLimit(int hisLimit) {
        userRepository.historyLimit = hisLimit;
    }

}
