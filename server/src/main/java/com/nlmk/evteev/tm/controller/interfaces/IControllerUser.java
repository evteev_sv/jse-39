package com.nlmk.evteev.tm.controller.interfaces;

import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IControllerUser extends IController {

    @WebMethod
    ResponseDTO create(final User user);

    @WebMethod
    ResponseDTO viewByName(@WebParam(name = "name") final String name) throws UserNotFoundException;

    @WebMethod
    ResponseDTO updateByIndex(@WebParam(name = "index") final int index, User user) throws UserNotFoundException;

    @WebMethod
    ResponseDTO updateById(@WebParam(name = "id") final long id, User user) throws UserNotFoundException;

    @WebMethod
    ResponseDTO updateByName(@WebParam(name = "name") final String name, User user) throws UserNotFoundException;

    @WebMethod
    ResponseDTO updatePasswordByLogin(@WebParam(name = "name") final String name, @WebParam(name = "password") String password) throws UserNotFoundException;

    @WebMethod
    ResponseDTO registry(@WebParam(name = "name") final String name, @WebParam(name = "password") String password) throws UserNotFoundException;

    @WebMethod
    ResponseDTO logOff();

    @WebMethod
    ResponseDTO updatePassword(@WebParam(name = "oldPassword") String oldPassword, @WebParam(name = "newPassword") String newPassword) throws UserNotFoundException;

    @WebMethod
    ResponseDTO displayUserInfo() throws UserNotFoundException;
}
