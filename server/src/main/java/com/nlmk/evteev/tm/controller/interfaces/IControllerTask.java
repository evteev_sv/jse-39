package com.nlmk.evteev.tm.controller.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IControllerTask extends IController {

    @WebMethod
    ResponseDTO create(final Task task);

    @WebMethod
    ResponseDTO updateById(@WebParam(name = "id") final long id, final Task task) throws TaskNotFoundException;

    @WebMethod
    ResponseDTO updateByIndex(@WebParam(name = "index") final int index, Task task) throws TaskNotFoundException;

    @WebMethod
    ListResponseDTO listTaskByProjectId(@WebParam(name = "id") final long projectId);

    @WebMethod
    ResponseDTO addTaskToProjectByIds(@WebParam(name = "projectId") final long projectId, @WebParam(name = "taskId") final long taskId) throws ProjectNotFoundException, TaskNotFoundException;

    @WebMethod
    ResponseDTO removeTaskFromProjectByIds(@WebParam(name = "projectId") final long projectId, @WebParam(name = "taskId") final long taskId);

    @WebMethod
    ListResponseDTO viewByName(@WebParam(name = "name") final String name) throws TaskNotFoundException;

}
