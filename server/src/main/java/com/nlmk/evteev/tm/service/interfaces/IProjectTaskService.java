package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;

public interface IProjectTaskService {
    ResponseDTO addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException;

    ResponseDTO removeTaskFromProject(final Long projectId, final Long taskId);

    void clear();

    ListResponseDTO findAllByProjectId(Long projectId);
}
