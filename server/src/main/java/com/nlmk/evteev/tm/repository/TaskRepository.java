package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.interfaces.ITaskRepository;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;


public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private static TaskRepository instance = null;
    private final UserRepository userRepository = UserRepository.getInstance();

    private TaskRepository() {
    }

    public static TaskRepository getInstance() {
        if (instance == null) {
            synchronized (TaskRepository.class) {
                if (instance == null) {
                    instance = new TaskRepository();
                }
            }
        }
        return instance;
    }

    public String getName(Task task) {
        if (task == null) return null;
        return task.getName();
    }

    public Long getUserId(Optional<Task> task) {
        return task.map(Task::getUserId).orElse(null);
    }

    @Override
    public Optional<Task> create(final Task task) {
        items.add(task);
        addToMap(task);
        startCountDeadLine(task);
        return Optional.of(task);
    }

    @Override
    public Optional<Task> update(final Task task1) throws TaskNotFoundException {
        Task task = findById(task1.getId()).orElse(null);
        if (task == null) return Optional.empty();
        String oldName = task.getName();
        var taskListOld = Optional.ofNullable(
                itemsA.get(oldName))
                .orElseThrow(() -> new TaskNotFoundException("Tasks are not found by name " + oldName + "."));
        if (taskListOld.isEmpty()) return Optional.empty();
        task.setName(task.getName());
        if (task.getDescription() != null) {
            task.setDescription(task.getDescription());
        }
        if (!oldName.equals(task.getName())) {
            taskListOld.remove(task);
            if (taskListOld.size() == 0) itemsA.remove(oldName);
            addToMap(task);
        }
        return Optional.of(task);
    }

    @Override
    public Optional<Task> findByIndex(int index) throws TaskNotFoundException {
        Optional<List<Task>> result;
        User user = userRepository.currentUser;
        if (user == null) {
            result = findAll();
        } else {
            result = findAllByUserId(user.getId());
        }
        result.orElseThrow(() -> new TaskNotFoundException("Task is not found by index " + index + "."));
        if (index < 0 || index > result.get().size() - 1) throw new TaskNotFoundException("Wrong index.");
        return Optional.ofNullable(result.get().get(index));
    }

    @Override
    public List<Task> findByName(final String name) throws TaskNotFoundException {
        if (!itemsA.containsKey(name)) throw new TaskNotFoundException("Tasks are not found by name " + name + ".");
        Optional<List<Task>> result = Optional.of(new ArrayList<>());
        User user = userRepository.currentUser;
        if (user == null) {
            result = Optional.ofNullable(itemsA.get(name));
        } else {
            itemsA.get(name).stream()
                    .filter(task -> task.getUserId().equals(user.getId()))
                    .forEach(result.get()::add);
        }
        result.orElseThrow(() -> new TaskNotFoundException("Tasks are not found by name " + name + "."));
        return result.orElse(Collections.emptyList());
    }

    @Override
    public Optional<Task> findById(final Long id) throws TaskNotFoundException {
        Optional<List<Task>> currentListTask;
        User user = userRepository.currentUser;
        if (user == null) {
            currentListTask = findAll();
        } else {
            currentListTask = findAllByUserId(user.getId());
        }
        currentListTask.orElseThrow(() -> new TaskNotFoundException("Task is not found by id " + id + "."));
        for (final Task task : currentListTask.get()) {
            if (task.getId().equals(id)) return Optional.of(task);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Task> findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : items) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return Optional.of(task);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Task> removeByIndex(final int index) throws TaskNotFoundException {
        Task task = findByIndex(index).orElse(null);
        if (task == null) return Optional.empty();
        items.remove(task);
        var taskFromName = Optional.ofNullable(
                itemsA.get(task.getName()))
                .orElseThrow(() -> new TaskNotFoundException("Tasks are not found by name " + task.getName() + "."));
        taskFromName.remove(task);
        return Optional.of(task);
    }

    @Override
    public Optional<Task> removeById(final Long id) throws TaskNotFoundException {
        Task task = findById(id).orElse(null);
        if (task == null) return Optional.empty();
        items.remove(task);
        var taskFromName = Optional.ofNullable(
                itemsA.get(task.getName()))
                .orElseThrow(() -> new TaskNotFoundException("Tasks are not found by name " + task.getName() + "."));
        taskFromName.remove(task);
        return Optional.of(task);
    }

    @Override
    public List<Task> removeByName(final String name) throws TaskNotFoundException {
        var taskList = findByName(name);
        var taskListFromName = Optional.ofNullable(
                itemsA.get(name))
                .orElseThrow(() -> new TaskNotFoundException("Tasks are not found by name " + name + "."));
        taskListFromName.removeAll(taskList);
        items.removeAll(taskList);
        return taskList;
    }

    @Override
    public Optional<List<Task>> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll().orElse(new ArrayList<>())) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return Optional.of(result);
    }

    private void startCountDeadLine(Task task) {
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(), task.getDeadline());
            long secondsToCheck = (diff + 1) / 2;
            while (diff > 0) {
                diff = ChronoUnit.SECONDS.between(LocalDateTime.now(), task.getDeadline());
                if (diff % secondsToCheck == 0) {
                    System.out.println("Time for task " + task.getName() + " - " + DurationFormatUtils.formatDuration(TimeUnit.SECONDS.toMillis(diff), "**H:mm:ss**", true));
                    try {
                        if (secondsToCheck <= diff) {
                            TimeUnit.SECONDS.sleep(secondsToCheck - 1);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (secondsToCheck > 3600) {
                        secondsToCheck = secondsToCheck / 4;
                    }
                    if (secondsToCheck > 30) {
                        secondsToCheck = secondsToCheck / 2;
                    }

                }
            }
            return diff;
        });

        future.thenAccept(result -> {
            if (userRepository.currentUser != null) {
                synchronized (findAllByUserId(userRepository.currentUser.getId())) {
                    try {
                        removeById(task.getId());
                        System.out.println(task.getName() + " was removed.");
                    } catch (TaskNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


}
