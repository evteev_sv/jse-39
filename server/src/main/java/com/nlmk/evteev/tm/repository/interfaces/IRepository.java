package com.nlmk.evteev.tm.repository.interfaces;

import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.exception.UserNotFoundException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IRepository<E> {

    Optional<E> create(final E item) throws UserNotFoundException;

    Optional<List<E>> findAll();

    void clear();

    Optional<E> findByIndex(final int index) throws NotFoundException;

    Optional<E> findById(final Long id) throws NotFoundException;

    Optional<E> removeByIndex(final int index) throws NotFoundException;

    Optional<E> removeById(final Long id) throws NotFoundException;

    Optional<E> update(final E project1) throws NotFoundException;

    int saveJSON(String fileName) throws IOException;

    int saveXML(String fileName) throws IOException;

}
