package com.nlmk.evteev.tm.exception;

public class TaskNotFoundException extends NotFoundException {

    public TaskNotFoundException(String message) {
        super(message);
    }

}
