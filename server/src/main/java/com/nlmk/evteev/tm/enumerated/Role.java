package com.nlmk.evteev.tm.enumerated;

import static com.nlmk.evteev.tm.constant.TerminalConst.ADMIN_ACTIONS;
import static com.nlmk.evteev.tm.constant.TerminalConst.USER_ACTIONS;

public enum Role {
    USER("User", USER_ACTIONS),
    ADMIN("Administrator", ADMIN_ACTIONS);

    private final String displayName;

    private final String[] actions;

    Role(String displayName, String[] actions) {
        this.displayName = displayName;
        this.actions = actions;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String[] getActions() {
        return actions;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
