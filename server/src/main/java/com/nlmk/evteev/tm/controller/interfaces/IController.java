package com.nlmk.evteev.tm.controller.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.exception.NotFoundException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

import java.io.IOException;


@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IController {

    @WebMethod
    ResponseDTO viewById(@WebParam(name = "id") final Long id) throws NotFoundException;

    @WebMethod
    ResponseDTO viewByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    ResponseDTO removeByIndex(@WebParam(name = "index") final int index) throws NotFoundException;

    @WebMethod
    ResponseDTO removeById(@WebParam(name = "id") final long id) throws NotFoundException;

    @WebMethod
    ResponseDTO removeByName(@WebParam(name = "name") final String name) throws NotFoundException;

    @WebMethod
    ResponseDTO clear() throws NotFoundException;

    @WebMethod
    ListResponseDTO list();

    @WebMethod
    ResponseDTO saveJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ResponseDTO saveXML(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ResponseDTO uploadFromJSON(@WebParam(name = "fileName") final String fileName) throws IOException;

    @WebMethod
    ResponseDTO uploadFromXML(@WebParam(name = "fileName") final String fileName) throws IOException;
}
