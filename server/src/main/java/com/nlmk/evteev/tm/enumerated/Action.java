package com.nlmk.evteev.tm.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum Action {

    ABOUT("about"),
    HELP("help"),
    VERSION("version"),
    EXIT("exit"),
    HISTORY("history"),

    PROJECT_CREATE("project-create"),
    PROJECT_CLEAR("project-clear"),
    PROJECT_LIST("project-list"),
    PROJECT_VIEW_BY_INDEX("project-view-by-index"),
    PROJECT_VIEW_BY_ID("project-view-by-id"),
    PROJECT_VIEW_BY_NAME("project-view-by-name"),
    PROJECT_REMOVE_BY_INDEX("project-remove-by-index"),
    PROJECT_REMOVE_BY_ID("project-remove-by-id"),
    PROJECT_REMOVE_BY_NAME("project-remove-by-name"),
    PROJECT_UPDATE_BY_INDEX("project-update-by-index"),
    PROJECT_UPDATE_BY_ID("project-update-by-id"),

    TASK_CREATE("task-create"),
    TASK_CLEAR("task-clear"),
    TASK_LIST("task-list"),
    TASK_VIEW_BY_INDEX("task-view-by-index"),
    TASK_VIEW_BY_ID("task-view-by-id"),
    TASK_VIEW_BY_NAME("task-view-by-name"),
    TASK_REMOVE_BY_INDEX("task-remove-by-index"),
    TASK_REMOVE_BY_ID("task-remove-by-id"),
    TASK_REMOVE_BY_NAME("task-remove-by-name"),
    TASK_UPDATE_BY_INDEX("task-update-by-index"),
    TASK_UPDATE_BY_ID("task-update-by-id"),
    TASK_LIST_BY_PROJECT_ID("task-list-by-project-id"),
    TASK_ADD_TO_PROJECT_BY_IDS("task-add-to-project-task-by-ids"),
    TASK_REMOVE_FROM_PROJECT_BY_IDS("task-remove-from-project-by-ids"),

    LOG_ON("log-on"),
    LOG_OFF("log-off"),
    USER_INFO("user-info"),
    USER_UPDATE("user-update"),
    USER_UPDATE_PASSWORD("user-update-password"),

    USER_CREATE("user-create"),
    USER_CLEAR("user-clear"),
    USER_LIST("user-list"),
    USER_VIEW_BY_LOGIN("user-view-by-login"),
    USER_REMOVE_BY_LOGIN("user-remove-by-login"),
    USER_UPDATE_BY_LOGIN("user-update-by-login"),
    USER_PASSWORD_UPDATE_BY_LOGIN("user-password-update-by-login"),
    USER_VIEW_BY_ID("user-view-by-id"),
    USER_REMOVE_BY_ID("user-remove-by-id"),
    USER_UPDATE_BY_ID("user-update-by-id"),
    USER_PASSWORD_UPDATE_BY_ID("user-password-update-by-id"),

    TASKS_TO_FILE_JSON("tasks-save-json"),
    PROJECTS_TO_FILE_JSON("projects-save-json"),
    USERS_TO_FILE_JSON("users-save-json"),

    TASKS_TO_FILE_XML("tasks-save-xml"),
    PROJECTS_TO_FILE_XML("projects-save-xml"),
    USERS_TO_FILE_XML("users-save-xml"),

    TASKS_FROM_FILE_JSON("tasks-upload-json"),
    PROJECTS_FROM_FILE_JSON("projects-upload-json"),
    USERS_FROM_FILE_JSON("users-upload-json"),

    TASKS_FROM_FILE_XML("tasks-upload-xml"),
    PROJECTS_FROM_FILE_XML("projects-upload-xml"),
    USERS_FROM_FILE_XML("users-upload-xml"),

    EMPTY("");

    private final static Map<String, Action> map = new HashMap<>();

    static {
        for (Action action1 : Action.values()) {
            map.put(action1.getAction(), action1);
        }
    }

    private String action;

    Action(String action) {
        this.action = action;
    }

    public static Action findAction(String action) {
        return map.get(action);
    }

    public String getAction() {
        return action;
    }
}
