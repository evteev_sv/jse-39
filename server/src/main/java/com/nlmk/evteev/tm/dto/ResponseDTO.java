package com.nlmk.evteev.tm.dto;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerated.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO {

    private Status status;
    private User payloadUser;
    private Task payloadTask;
    private Project payloadProject;

}
