package com.nlmk.evteev.tm.exception;

public class ProjectNotFoundException extends NotFoundException {

    public ProjectNotFoundException(String message) {
        super(message);
    }

}

