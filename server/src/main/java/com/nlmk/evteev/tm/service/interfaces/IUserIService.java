package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.exception.UserNotFoundException;

import java.util.Deque;
import java.util.Optional;

public interface IUserIService extends IService<User> {
    ResponseDTO updateByName(final String name, final User user) throws NotFoundException;

    boolean checkPassword(final Optional<User> user, final String password);

    ResponseDTO updatePasswordByLogin(String login, String password) throws UserNotFoundException;

    ResponseDTO updatePasswordById(Long id, String password) throws UserNotFoundException;

    User getCurrentUser();

    void setCurrentUser(User user);

    Deque<String> getHistory();

    int addCommandToHistory(String command);

    int getHistoryLimit();

    void setHistoryLimit(int hisLimit);

    ResponseDTO findByName(final String name) throws UserNotFoundException;

    ResponseDTO removeByName(final String name) throws UserNotFoundException;
}
