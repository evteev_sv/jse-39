package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;

public interface IProjectIService extends IService<Project> {
    ListResponseDTO findAllByUserId(Long Id);

    ListResponseDTO findByName(final String name) throws ProjectNotFoundException;

    ListResponseDTO removeByName(final String name) throws ProjectNotFoundException;
}
