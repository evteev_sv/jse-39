package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.service.interfaces.IProjectIService;
import com.nlmk.evteev.tm.util.Helper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class ProjectService implements IProjectIService {

    private static ProjectService instance = null;
    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private ProjectService() {
    }

    public static ProjectService getInstance() {
        if (instance == null) {
            synchronized (ProjectService.class) {
                if (instance == null) {
                    instance = new ProjectService();
                }
            }
        }
        return instance;
    }

    @Override
    public ResponseDTO create(final Project project) {
        String name = project.getName();
        if (name.equals("") || Helper.checkProjectName(name) || name.isEmpty()) {
            return ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<Project> projectOptional = projectRepository.create(project);
        if (projectOptional.isPresent()) {
            return ResponseDTO.builder().payloadProject(projectOptional.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateByIndex(final int index, Project project) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.findByIndex(index - 1);
        if (project1.isPresent()) {
            Project updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(project.getName())
                    .description(project.getDescription())
                    .userId(project.getUserId()).build();
            project1 = projectRepository.update(updatedProject);
            if (project1.isPresent()) {
                return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO updateById(final long id, Project project) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.findById(id);
        if (project1.isPresent()) {
            Project updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(project.getName())
                    .description(project.getDescription())
                    .userId(project.getUserId()).build();
            project1 = projectRepository.update(updatedProject);
            if (project1.isPresent()) {
                return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
            }
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public ResponseDTO findByIndex(final int index) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.findByIndex(index - 1);
        if (project1.isPresent()) {
            return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO findByName(final String name) throws ProjectNotFoundException {
        List<Project> project1 = projectRepository.findByName(name);
        if (project1.isEmpty()) {
            return ListResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListResponseDTO
                .builder()
                .payloadProject(project1.toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ResponseDTO findById(final Long id) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.findById(id);
        if (project1.isPresent()) {
            return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeByIndex(final Integer index) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.removeByIndex(index);
        if (project1.isPresent()) {
            return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ResponseDTO removeById(final Long id) throws ProjectNotFoundException {
        Optional<Project> project1 = projectRepository.removeById(id);
        if (project1.isPresent()) {
            return ResponseDTO.builder().payloadProject(project1.get()).status(Status.OK).build();
        }
        return ResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO removeByName(final String name) throws ProjectNotFoundException {
        return ListResponseDTO
                .builder()
                .payloadProject(projectRepository.removeByName(name).toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public ListResponseDTO findAll() {
        return ListResponseDTO
                .builder()
                .payloadProject(projectRepository.findAll().get().toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListResponseDTO findAllByUserId(Long userId) {
        if (userId == null) return ListResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListResponseDTO
                .builder()
                .payloadProject(projectRepository.findAllByUserId(userId).get().toArray(new Project[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListResponseDTO saveJSON(final String fileName) throws IOException {
        int status = projectRepository.saveJSON(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO saveXML(final String fileName) throws IOException {
        int status = projectRepository.saveXML(fileName);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromJSON(final String fileName) throws IOException {
        int status = projectRepository.uploadJSON(fileName, Project.class);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListResponseDTO uploadFromXML(final String fileName) throws IOException {
        int status = projectRepository.uploadXML(fileName, Project.class);
        if (status == 0) {
            return ListResponseDTO.builder().status(Status.OK).build();
        }
        return ListResponseDTO.builder().status(Status.DB_ERROR).build();
    }
}
