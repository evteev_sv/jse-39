package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Status;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.interfaces.IProjectTaskService;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private static ProjectTaskService instance = null;
    private final ProjectRepository projectRepository = ProjectRepository.getInstance();
    private final TaskRepository taskRepository = TaskRepository.getInstance();

    private ProjectTaskService() {
    }

    public static ProjectTaskService getInstance() {
        if (instance == null) instance = new ProjectTaskService();
        return instance;
    }

    @Override
    public ResponseDTO removeTaskFromProject(final Long projectId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        task.get().setProjectId(null);
        return ResponseDTO.builder().payloadTask(task.get()).status(Status.OK).build();
    }

    @Override
    public ResponseDTO addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Optional<Project> project = projectRepository.findById(projectId);
        if (project.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        final Optional<Task> task = taskRepository.findById(taskId);
        if (task.isEmpty()) return ResponseDTO.builder().status(Status.DB_ERROR).build();
        task.get().setProjectId(projectId);
        return ResponseDTO.builder().payloadTask(task.get()).status(Status.OK).build();
    }

    @Override
    public ListResponseDTO findAllByProjectId(Long projectId) {
        Optional<List<Task>> listTask = taskRepository.findAllByProjectId(projectId);
        if (listTask.isEmpty()) {
            ResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListResponseDTO
                .builder()
                .payloadTask(listTask.get().stream().toArray(Task[]::new))
                .status(Status.OK).build();
    }

    @Override
    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
