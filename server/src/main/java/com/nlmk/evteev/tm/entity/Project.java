package com.nlmk.evteev.tm.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
public class Project implements Serializable {

    public static final Long serialVersionUID = 1L;

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long userId;

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name, String description, Long userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    public Project(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Project(Long id, String name, String description, Long userId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

}
