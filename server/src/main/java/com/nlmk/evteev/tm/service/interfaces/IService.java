package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.dto.ListResponseDTO;
import com.nlmk.evteev.tm.dto.ResponseDTO;
import com.nlmk.evteev.tm.exception.NotFoundException;

import java.io.IOException;

public interface IService<T> {
    ResponseDTO create(final T item);

    ResponseDTO updateById(final long id, T item) throws NotFoundException;

    ResponseDTO updateByIndex(final int index, T item) throws NotFoundException;

    ResponseDTO findByIndex(final int index) throws NotFoundException;

    ResponseDTO findById(final Long id) throws NotFoundException;

    ResponseDTO removeById(final Long id) throws NotFoundException;

    ResponseDTO removeByIndex(final Integer index) throws NotFoundException;

    void clear();

    ListResponseDTO findAll();

    ListResponseDTO saveJSON(final String fileName) throws IOException;

    ListResponseDTO saveXML(final String fileName) throws IOException;

    ListResponseDTO uploadFromJSON(final String fileName) throws IOException;

    ListResponseDTO uploadFromXML(final String fileName) throws IOException;

}
