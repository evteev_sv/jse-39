package com.nlmk.evteev.tm.repository.interfaces;

import com.nlmk.evteev.tm.entity.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends ITaskProjectRepository<Task> {

    Optional<Task> findByProjectIdAndId(final Long projectId, final Long id);

    Optional<List<Task>> findAllByProjectId(final Long projectId);

}
