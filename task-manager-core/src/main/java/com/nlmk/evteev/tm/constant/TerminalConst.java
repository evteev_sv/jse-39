package com.nlmk.evteev.tm.constant;

public class TerminalConst {
    public static final String ABOUT = "about";
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String EXIT = "exit";
    public static final String HISTORY = "history";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_VIEW_BY_NAME = "task-view-by-name";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-task-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";

    public static final String LOG_ON = "log-on";
    public static final String LOG_OFF = "log-off";
    public static final String USER_INFO = "user-info";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_UPDATE_PASSWORD = "user-update-password";

    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_PASSWORD_UPDATE_BY_LOGIN = "user-password-update-by-login";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";
    public static final String USER_PASSWORD_UPDATE_BY_ID = "user-password-update-by-id";

    public static final String TASKS_TO_FILE_JSON = "tasks-save-json";
    public static final String PROJECTS_TO_FILE_JSON = "projects-save-json";
    public static final String USERS_TO_FILE_JSON = "users-save-json";

    public static final String TASKS_TO_FILE_XML = "tasks-save-xml";
    public static final String PROJECTS_TO_FILE_XML = "projects-save-xml";
    public static final String USERS_TO_FILE_XML = "users-save-xml";

    public static final String TASKS_FROM_FILE_JSON = "tasks-upload-json";
    public static final String PROJECTS_FROM_FILE_JSON = "projects-upload-json";
    public static final String USERS_FROM_FILE_JSON = "users-upload-json";

    public static final String TASKS_FROM_FILE_XML = "tasks-upload-xml";
    public static final String PROJECTS_FROM_FILE_XML = "projects-upload-xml";
    public static final String USERS_FROM_FILE_XML = "users-upload-xml";

    public static final String TASKS_FILE_NAME_JSON = "tasks.json";
    public static final String PROJECTS_FILE_NAME_JSON = "projects.json";
    public static final String USERS_FILE_NAME_JSON = "users.json";

    public static final String TASKS_FILE_NAME_XML = "tasks.xml";
    public static final String PROJECTS_FILE_NAME_XML = "projects.xml";
    public static final String USERS_FILE_NAME_XML = "users.xml";

    public static String[] ALL_ACTIONS = new String[]{
            USER_CREATE, LOG_ON, LOG_OFF,
            ABOUT, HELP, VERSION, EXIT, HISTORY
    };

    public static String[] ADMIN_ACTIONS = new String[]{
            LOG_ON, LOG_OFF, USER_INFO, USER_UPDATE, USER_UPDATE_PASSWORD,
            USER_CREATE, USER_CLEAR, USER_LIST, HISTORY,
            USER_VIEW_BY_LOGIN, USER_VIEW_BY_ID,
            USER_REMOVE_BY_LOGIN, USER_REMOVE_BY_ID,
            USER_UPDATE_BY_LOGIN, USER_UPDATE_BY_ID,
            USER_PASSWORD_UPDATE_BY_LOGIN, USER_PASSWORD_UPDATE_BY_ID,
            ABOUT, HELP, VERSION, EXIT,
            PROJECT_CREATE, TASK_CREATE,
            PROJECT_CLEAR, TASK_CLEAR,
            PROJECT_LIST, TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_VIEW_BY_NAME, TASK_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_INDEX, TASK_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, TASK_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_INDEX, TASK_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            TASK_LIST_BY_PROJECT_ID,
            TASK_ADD_TO_PROJECT_BY_IDS,
            TASK_REMOVE_FROM_PROJECT_BY_IDS,
            TASKS_TO_FILE_XML, PROJECTS_TO_FILE_XML, USERS_TO_FILE_XML,
            TASKS_TO_FILE_JSON, PROJECTS_TO_FILE_JSON, USERS_TO_FILE_JSON,
            TASKS_FROM_FILE_XML, PROJECTS_FROM_FILE_XML, USERS_FROM_FILE_XML,
            TASKS_FROM_FILE_JSON, PROJECTS_FROM_FILE_JSON, USERS_FROM_FILE_JSON,
    };

    public static String[] USER_ACTIONS = new String[]{
            LOG_ON, LOG_OFF,
            USER_CREATE, USER_UPDATE_PASSWORD,
            USER_INFO, USER_UPDATE, HISTORY,
            USER_PASSWORD_UPDATE_BY_LOGIN, USER_PASSWORD_UPDATE_BY_ID,
            ABOUT, HELP, VERSION, EXIT,
            PROJECT_CREATE, TASK_CREATE,
            PROJECT_CLEAR, TASK_CLEAR,
            PROJECT_LIST, TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_VIEW_BY_NAME, TASK_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_INDEX, TASK_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, TASK_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_INDEX, TASK_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            TASK_LIST_BY_PROJECT_ID,
            TASK_ADD_TO_PROJECT_BY_IDS,
            TASK_REMOVE_FROM_PROJECT_BY_IDS
    };

}
