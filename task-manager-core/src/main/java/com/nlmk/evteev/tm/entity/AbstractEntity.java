package com.nlmk.evteev.tm.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = -8641004601575326459L;

    Long id;

    String name;
}
