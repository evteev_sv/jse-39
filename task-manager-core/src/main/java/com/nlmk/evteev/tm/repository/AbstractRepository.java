package com.nlmk.evteev.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.repository.interfaces.ITaskProjectRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;


public abstract class AbstractRepository<T extends AbstractEntity> implements ITaskProjectRepository<T> {

    public CopyOnWriteArrayList<T> items = new CopyOnWriteArrayList<>();

    public ConcurrentHashMap<String, List<T>> itemsA = new ConcurrentHashMap<>();

    @Override
    public abstract String getName(T item);

    @Override
    public abstract Long getUserId(Optional<T> item);

    @Override
    public Optional<T> create(final T item) {
        items.add(item);
        addToMap(item);
        return Optional.of(item);
    }

    @Override
    public void addToMap(final T item) {
        List<T> itemsInMap = itemsA.get(getName(item));
        if (itemsInMap == null) itemsInMap = new ArrayList<>();
        itemsInMap.add(item);
        itemsA.put(getName(item), itemsInMap);
    }

    @Override
    public Optional<List<T>> findAll() {
        return Optional.of(items);
    }

    @Override
    public void clear() {
        items.clear();
        itemsA.clear();
    }

    @Override
    public int size(final Long userId) {
        if (userId == null) {
            return findAll().orElse(new ArrayList<>()).size();
        } else {
            return findAllByUserId(userId).orElse(new ArrayList<>()).size();
        }
    }

    @Override
    public Optional<List<T>> findAllByUserId(final Long userId) {
        final List<T> result = new ArrayList<>();
        for (final T item : findAll().orElse(new ArrayList<>())) {
            final Long IdUser = getUserId(Optional.of(item));
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(item);
        }
        return Optional.of(result);
    }

    @Override
    public int saveJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        final File fileA = new File("A" + fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, items);
        objectMapper.writeValue(fileA, itemsA);
        return 0;
    }

    @Override
    public int saveXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File("A" + fileName), itemsA);
        xmlMapper.writeValue(new File(fileName), items);
        return 0;
    }

    @Override
    public int uploadJSON(String fileName, Class<T> clazz) throws IOException {
        final File file = new File(fileName);
        final File fileA = new File("A" + fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        clear();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        JavaType type = typeFactory.constructCollectionType(List.class, clazz);
        JavaType typeString = typeFactory.constructType(String.class);
        MapType mapType = typeFactory.constructMapType(HashMap.class, typeString, type);
        items = objectMapper.readValue(file, type);
        itemsA = objectMapper.readValue(fileA, mapType);
        if (checkItemsOfCollections()) {
            itemsA.clear();
            for (T item : items) {
                addToMap(item);
            }
        }
        return 0;
    }

    @Override
    public int uploadXML(String fileName, Class<T> clazz) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, clazz);
        JavaType typeString = xmlMapper.getTypeFactory().constructType(String.class);
        MapType mapType = xmlMapper.getTypeFactory().constructMapType(HashMap.class, typeString, type);
        clear();
        items = xmlMapper.readValue(new File(fileName), type);
        itemsA = xmlMapper.readValue(new File("A" + fileName), mapType);
        if (checkItemsOfCollections()) {
            itemsA.clear();
            for (T item : items) {
                addToMap(item);
            }
        }
        return 0;
    }

    private boolean checkItemsOfCollections() {
        for (T item : items) {
            if (itemsA.get(getName(item)) == null) {
                return true;
            }
            if (!itemsA.get(getName(item)).contains(item)) {
                return true;
            }
        }
        return false;
    }

}
