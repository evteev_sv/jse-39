package com.nlmk.evteev.tm.repository.interfaces;

import java.io.IOException;
import java.util.Optional;

public interface IUserRepository<User> extends IRepository<User> {

    int uploadJSON(String fileName) throws IOException;

    int uploadXML(String fileName) throws IOException;

    int addCommandToHistory(String command);

    Optional<User> removeByName(final String login);

    Optional<User> updatePassword(final User user1);

    Optional<User> updateRole(final User user1);

    Optional<User> findByName(final String login);

}
