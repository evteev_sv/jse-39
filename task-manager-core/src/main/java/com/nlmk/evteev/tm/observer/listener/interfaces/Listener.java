package com.nlmk.evteev.tm.observer.listener.interfaces;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.exception.NotFoundException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

public interface Listener {
    static String getNameFromParameters(List<String> parameters) {
        if (parameters.size() > 0) {
            return parameters.get(0);
        } else {
            return "";
        }
    }

    static int viewMultiple(final AbstractEntity[] items) {
        if (items.length == 0) {
            System.out.println("[FAIL]");
            return -1;
        }
        int index = 1;
        Arrays.sort(items, Comparator.comparing(AbstractEntity::getName));
        for (final AbstractEntity item : items) {
            System.out.println(index + ". " + item.getId() + ": " + item.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    static int inputIndexCheckFormat(String input) {
        final int index;
        try {
            index = Integer.parseInt(input) - 1;
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return -1;
        }
        return index;
    }

    static long inputIdCheckFormat(String command) {
        final long id;
        try {
            id = Long.parseLong(command);
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

    static boolean checkProjectName(String name) {
        if (Pattern.compile("^\\d").matcher(name).lookingAt()) {
            System.out.println("[NAMES SHOULD NOT START FROM NUMBERS]");
            return true;
        }
        return false;
    }

    int update(String command) throws IOException, NotFoundException;

    int create(List<String> parameters);

    int updateByIndex(List<String> parameters) throws NotFoundException;

    int clear() throws NotFoundException;

    int removeByName(List<String> parameters) throws NotFoundException;

    void view(final Long id) throws NotFoundException;

    int viewById(List<String> parameters) throws NotFoundException;

    int list();

    int saveJSON(final String fileName) throws IOException;

    int saveXML(final String fileName) throws IOException;

    int uploadFromJSON(final String fileName) throws IOException;

    int uploadFromXML(final String fileName) throws IOException;

}
