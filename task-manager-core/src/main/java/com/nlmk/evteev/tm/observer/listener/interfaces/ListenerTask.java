package com.nlmk.evteev.tm.observer.listener.interfaces;

import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;

import java.util.List;

public interface ListenerTask extends Listener {

    int listTaskByProjectId(List<String> parameters);

    int addTaskToProjectByIds(List<String> parameters) throws ProjectNotFoundException, TaskNotFoundException;

    int removeTaskFromProjectByIds(List<String> parameters);

}
