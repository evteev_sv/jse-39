package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.interfaces.IProjectTaskService;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class ProjectTaskService implements IProjectTaskService {

    private static ProjectTaskService instance = null;
    private final ProjectRepository projectRepository = ProjectRepository.getInstance();
    private final TaskRepository taskRepository = TaskRepository.getInstance();

    public static ProjectTaskService getInstance() {
        if (instance == null) instance = new ProjectTaskService();
        return instance;
    }

    @Override
    public void removeTaskFromProject(final Long projectId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task.isEmpty()) return;
        task.get().setProjectId(null);
    }

    @Override
    public Optional<Task> addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Optional<Project> project = projectRepository.findById(projectId);
        if (project.isEmpty()) return Optional.empty();
        final Optional<Task> task = taskRepository.findById(taskId);
        if (task.isEmpty()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @Override
    public Optional<List<Task>> findAllByProjectId(Long projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
