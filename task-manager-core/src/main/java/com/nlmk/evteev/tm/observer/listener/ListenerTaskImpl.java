package com.nlmk.evteev.tm.observer.listener;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Action;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.observer.listener.interfaces.Listener;
import com.nlmk.evteev.tm.observer.listener.interfaces.ListenerTask;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.service.UserService;
import com.nlmk.evteev.tm.util.RequestParser;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

@Log4j2
public class ListenerTaskImpl implements ListenerTask {

    private final TaskService taskService;

    private final UserService userService;

    private final ProjectTaskService projectTaskService;

    public ListenerTaskImpl() {
        this.userService = UserService.getInstance();
        this.taskService = TaskService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
    }

    @Override
    public int update(String param) throws TaskNotFoundException, ProjectNotFoundException, IOException {
        Action action = RequestParser.getAction(param);
        List<String> parameters = RequestParser.getParamsList(RequestParser.getParams(param));
        switch (action.getAction()) {
            case TASK_CREATE:
                return create(parameters);
            case TASK_CLEAR:
                return clear();
            case TASK_LIST:
                return list();
            case TASK_VIEW_BY_ID:
                return viewById(parameters);
            case TASK_REMOVE_BY_NAME:
                return removeByName(parameters);
            case TASK_UPDATE_BY_INDEX:
                return updateByIndex(parameters);
            case TASK_LIST_BY_PROJECT_ID:
                return listTaskByProjectId(parameters);
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return addTaskToProjectByIds(parameters);
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return removeTaskFromProjectByIds(parameters);
            case TASKS_TO_FILE_XML:
                return saveXML(TASKS_FILE_NAME_XML);
            case TASKS_TO_FILE_JSON:
                return saveJSON(TASKS_FILE_NAME_JSON);
            case TASKS_FROM_FILE_XML:
                return uploadFromXML(TASKS_FILE_NAME_XML);
            case TASKS_FROM_FILE_JSON:
                return uploadFromJSON(TASKS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }

    @Override
    public int create(List<String> parameters) {
        System.out.println("[CREATE TASK]");
        String name = Listener.getNameFromParameters(parameters);
        String description = "";
        if (name.equals("")) {
            return -1;
        }
        if (Listener.checkProjectName(name)) {
            return -1;
        }
        if (parameters.size() > 1) {
            description = parameters.get(1);
        }
        if (userService.getCurrentUser() == null) taskService.create(name, description);
        else taskService.create(name, description, userService.getCurrentUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int updateByIndex(List<String> parameters) throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        String name, description;
        int index;
        if (parameters.size() > 1) {
            index = Listener.inputIndexCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        final Long userId = userService.getCurrentUser().getId();
        final var task = taskService.findByIndex(index);
        if (task.isEmpty()) {
            System.out.println("[FAIL]");
            return -1;
        }
        name = parameters.get(1);
        if (Listener.checkProjectName(name)) {
            return -1;
        }
        if (parameters.size() > 1) {
            description = parameters.get(2);
            taskService.update(task.get().getId(), name, description, userId);
        } else {
            taskService.update(task.get().getId(), name, userId);
        }
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int clear() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK]");
        if (userService.getCurrentUser() == null) {
            taskService.clear();
            projectTaskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.getCurrentUser().getId()).orElse(new ArrayList<>())) {
                taskService.removeById(task.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int removeByName(List<String> parameters) throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY NAME]");
        String name = Listener.getNameFromParameters(parameters);
        if (name.equals("")) {
            return -1;
        }
        final var task = taskService.removeByName(name);
        if (task.isEmpty()) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public void view(final Long id) throws TaskNotFoundException {
        final var task = taskService.findById(id);
        if (task.isEmpty()) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.get().getId());
        System.out.println("NAME: " + task.get().getName());
        System.out.println("DESCRIPTION: " + task.get().getDescription());
        System.out.println("[OK]");
    }

    @Override
    public int viewById(List<String> parameters) throws TaskNotFoundException {
        long id;
        if (parameters.size() > 0) {
            id = Listener.inputIdCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        view(id);
        return 0;
    }

    @Override
    public int list() {
        System.out.println("[LIST TASK]");
        Optional<List<Task>> taskList;
        if (userService.getCurrentUser() == null) taskList = taskService.findAll();
        else taskList = taskService.findAllByUserId(userService.getCurrentUser().getId());
        return Listener.viewMultiple(taskList
                .orElse(Collections.emptyList())
                .toArray(AbstractEntity[]::new));
    }

    @Override
    public int listTaskByProjectId(List<String> parameters) {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        long id;
        if (parameters.size() > 0) {
            id = Listener.inputIdCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        final var tasks = taskService.findAllByProjectId(id);
        return Listener.viewMultiple(tasks
                .orElse(Collections.emptyList())
                .toArray(AbstractEntity[]::new));
    }

    @Override
    public int addTaskToProjectByIds(List<String> parameters) throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        long projectId, taskId;
        if (parameters.size() > 1) {
            projectId = Listener.inputIdCheckFormat(parameters.get(0));
            taskId = Listener.inputIdCheckFormat(parameters.get(1));
        } else {
            return -1;
        }
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int removeTaskFromProjectByIds(List<String> parameters) {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        long projectId, taskId;
        if (parameters.size() > 1) {
            projectId = Listener.inputIdCheckFormat(parameters.get(0));
            taskId = Listener.inputIdCheckFormat(parameters.get(1));
        } else {
            return -1;
        }
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

}

