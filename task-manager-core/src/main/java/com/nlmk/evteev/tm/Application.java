package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.observer.listener.ListenerProjectImpl;
import com.nlmk.evteev.tm.observer.listener.ListenerTaskImpl;
import com.nlmk.evteev.tm.observer.listener.ListenerUserImpl;
import com.nlmk.evteev.tm.observer.publisher.PublisherImpl;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.service.UserService;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Scanner;

@Log4j2
public class Application {

    static {
        UserService.getInstance().create("admin", "123", "name1", "ivnov", Role.ADMIN);
        UserService.getInstance().create("test", "123", "name2", "petrov", Role.USER);
        ProjectService.getInstance().create("Cproject1", "", UserService.getInstance().findByName("admin").get().getId());
        ProjectService.getInstance().create("Broject2", "", UserService.getInstance().findByName("admin").get().getId());
        ProjectService.getInstance().create("Aroject2", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "1fg", UserService.getInstance().findByName("test").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task2", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("Xtask2", "", UserService.getInstance().findByName("admin").get().getId());
    }

    public static void main(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        var publisher = new PublisherImpl(UserService.getInstance());
        var listenerUser = new ListenerUserImpl();
        var listenerProject = new ListenerProjectImpl();
        var listenerTask = new ListenerTaskImpl();
        publisher.addListener(listenerUser);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        try {
            publisher.start(new Scanner(System.in));
        } catch (NotFoundException | IOException exception) {
            log.error(exception.getMessage());
        }
    }

}
