package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IProjectIService extends IService<Project> {

    Optional<Project> create(final String name);

    Optional<Project> create(final String name, final String description);

    Optional<Project> create(final String name, final String description, final Long userId);

    Optional<Project> update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException;

    Optional<Project> update(final Long id, final String name, final Long userId) throws ProjectNotFoundException;

    Optional<List<Project>> findAllByUserId(Long Id);

    Optional<List<Project>> findByName(final String name) throws ProjectNotFoundException;

    Optional<List<Project>> removeByName(final String name) throws ProjectNotFoundException;

}
