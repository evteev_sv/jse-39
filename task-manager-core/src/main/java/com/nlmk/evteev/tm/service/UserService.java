package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.interfaces.IUserIService;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Deque;
import java.util.List;
import java.util.Optional;

import static com.nlmk.evteev.tm.util.HashUtil.hashMD5;

@NoArgsConstructor
public class UserService implements IUserIService {

    private static UserService instance = null;
    private final UserRepository userRepository = UserRepository.getInstance();

    public static UserService getInstance() {
        if (instance == null) {
            synchronized (UserService.class) {
                if (instance == null) {
                    instance = new UserService();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<User> create(
            String login, String password,
            String firstName, String lastName, Role role) {
        if (login == null || login.isEmpty()) return Optional.empty();
        if (password == null) return Optional.empty();
        if (firstName == null) return Optional.empty();
        if (lastName == null) return Optional.empty();
        if (role == null) return Optional.empty();
        User user = User.builder()
                .id(System.nanoTime())
                .name(login)
                .password(hashMD5(password))
                .firstName(firstName)
                .lastName(lastName)
                .role(role).build();
        return userRepository.create(user);
    }

    @Override
    public Optional<User> update(String login, String password, String firstName, String lastName) {
        if (login == null || login.isEmpty()) return Optional.empty();
        if (password == null) return Optional.empty();
        if (firstName == null) return Optional.empty();
        if (lastName == null) return Optional.empty();
        User user = User.builder()
                .name(login)
                .password(hashMD5(password))
                .firstName(firstName)
                .lastName(lastName)
                .build();
        return userRepository.update(user);
    }

    @Override
    public Optional<User> update(Long id, String password, String firstName, String lastName) {
        if (id == null) return Optional.empty();
        if (password == null) return Optional.empty();
        if (firstName == null) return Optional.empty();
        if (lastName == null) return Optional.empty();
        User user = User.builder()
                .id(id)
                .password(hashMD5(password))
                .firstName(firstName)
                .lastName(lastName)
                .build();
        return userRepository.update(user);
    }

    @Override
    public Optional<User> updatePasswordByLogin(String login, String password) {
        if (login == null || login.isEmpty()) return Optional.empty();
        if (password == null) return Optional.empty();
        User user = User.builder()
                .name(login)
                .password(hashMD5(password))
                .build();
        return userRepository.updatePassword(user);
    }

    @Override
    public Optional<User> updatePasswordById(Long id, String password) {
        if (id == null) return Optional.empty();
        if (password == null) return Optional.empty();
        User user = User.builder()
                .id(id)
                .password(hashMD5(password))
                .build();
        return userRepository.updatePassword(user);
    }

    @Override
    public boolean checkPassword(final Optional<User> user, final String password) {
        return user.map(value -> value.getPassword().equals(hashMD5(password))).orElse(false);
    }

    @Override
    public Optional<User> findByName(String login) {
        if (login == null || login.isEmpty()) return Optional.empty();
        return userRepository.findByName(login);
    }

    @Override
    public Optional<User> findById(Long id) {
        if (id == null) return Optional.empty();
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByIndex(final int index) throws UserNotFoundException {
        return userRepository.findByIndex(index);
    }

    @Override
    public Optional<User> removeByName(String login) {
        if (login == null || login.isEmpty()) return Optional.empty();
        return userRepository.removeByName(login);
    }

    @Override
    public Optional<User> removeById(Long id) {
        if (id == null) return Optional.empty();
        return userRepository.removeById(id);
    }

    @Override
    public Optional<User> removeByIndex(Integer index) throws UserNotFoundException {
        return userRepository.removeByIndex(index);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public Optional<List<User>> findAll() {
        return userRepository.findAll();
    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        return userRepository.saveJSON(fileName);
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        return userRepository.saveXML(fileName);
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        return userRepository.uploadJSON(fileName);
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        return userRepository.uploadXML(fileName);
    }

    @Override
    public User getCurrentUser() {
        return userRepository.currentUser;
    }

    @Override
    public void setCurrentUser(User user) {
        userRepository.currentUser = user;
    }

    @Override
    public Deque<String> getHistory() {
        return userRepository.history;
    }

    @Override
    public int addCommandToHistory(String command) {
        return userRepository.addCommandToHistory(command);
    }

    @Override
    public int getHistoryLimit() {
        return userRepository.historyLimit;
    }

    @Override
    public void setHistoryLimit(int hisLimit) {
        userRepository.historyLimit = hisLimit;
    }

}
