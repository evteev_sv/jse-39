package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    Optional<Task> addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException;

    void removeTaskFromProject(final Long projectId, final Long taskId);

    void clear();

    Optional<List<Task>> findAllByProjectId(Long projectId);

}
