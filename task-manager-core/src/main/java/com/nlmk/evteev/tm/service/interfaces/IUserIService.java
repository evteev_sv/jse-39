package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.exception.UserNotFoundException;

import java.util.Deque;
import java.util.Optional;

public interface IUserIService extends IService<User> {

    Optional<User> create(String login, String password,
                          String firstName, String lastName, Role role);

    boolean checkPassword(final Optional<User> user, final String password);

    Optional<User> update(String login, String password, String firstName, String lastName);

    Optional<User> update(Long id, String password, String firstName, String lastName);

    Optional<User> updatePasswordByLogin(String login, String password);

    Optional<User> updatePasswordById(Long id, String password);

    User getCurrentUser();

    void setCurrentUser(User user);

    Deque<String> getHistory();

    int addCommandToHistory(String command);

    int getHistoryLimit();

    void setHistoryLimit(int hisLimit);

    Optional<User> findByName(final String name) throws UserNotFoundException;

    Optional<User> removeByName(final String name) throws UserNotFoundException;
}
