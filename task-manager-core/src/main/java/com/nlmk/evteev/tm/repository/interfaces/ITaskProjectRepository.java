package com.nlmk.evteev.tm.repository.interfaces;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.exception.NotFoundException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ITaskProjectRepository<E extends AbstractEntity> extends IRepository<E> {

    Optional<List<E>> findAllByUserId(final Long userId);

    Long getUserId(Optional<E> project);

    String getName(E project);

    int uploadJSON(String fileName, Class<E> clazz) throws IOException;

    int uploadXML(String fileName, Class<E> clazz) throws IOException;

    Optional<List<E>> removeByName(final String name) throws NotFoundException;

    Optional<List<E>> findByName(final String name) throws NotFoundException;

    void addToMap(final E item);

    int size(final Long userId);

}
