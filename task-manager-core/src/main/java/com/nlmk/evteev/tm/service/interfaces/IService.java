package com.nlmk.evteev.tm.service.interfaces;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.exception.NotFoundException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IService<T extends AbstractEntity> {

    Optional<T> findByIndex(final int index) throws NotFoundException;

    Optional<T> findById(final Long id) throws NotFoundException;

    Optional<T> removeById(final Long id) throws NotFoundException;

    Optional<T> removeByIndex(final Integer index) throws NotFoundException;

    void clear();

    Optional<List<T>> findAll();

    int saveJSON(final String fileName) throws IOException;

    int saveXML(final String fileName) throws IOException;

    int uploadFromJSON(final String fileName) throws IOException;

    int uploadFromXML(final String fileName) throws IOException;

}
