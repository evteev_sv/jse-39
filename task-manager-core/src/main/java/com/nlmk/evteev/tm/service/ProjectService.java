package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.service.interfaces.IProjectIService;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class ProjectService implements IProjectIService {

    private static ProjectService instance = null;
    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    public static ProjectService getInstance() {
        if (instance == null) {
            synchronized (ProjectService.class) {
                if (instance == null) {
                    instance = new ProjectService();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<Project> create(final String name) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Project project1 = Project.builder()
                .id(System.nanoTime())
                .name(name).build();
        return projectRepository.create(project1);
    }

    @Override
    public Optional<Project> create(final String name, final String description) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Project project1 = Project.builder()
                .id(System.nanoTime())
                .name(name)
                .description(description)
                .build();
        return projectRepository.create(project1);
    }

    @Override
    public Optional<Project> create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Project project1 = Project.builder()
                .id(System.nanoTime())
                .name(name)
                .description(description)
                .userId(userId)
                .build();
        return projectRepository.create(project1);
    }

    @Override
    public Optional<Project> update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        if (id == null) return Optional.empty();
        if (name == null || name.isEmpty()) return Optional.empty();
        Project project1 = Project.builder()
                .id(id)
                .name(name)
                .description(description)
                .userId(userId)
                .build();
        return projectRepository.update(project1);
    }

    @Override
    public Optional<Project> update(final Long id, final String name, final Long userId) throws ProjectNotFoundException {
        if (id == null) return Optional.empty();
        if (name == null || name.isEmpty()) return Optional.empty();
        Project project1 = Project.builder()
                .id(id)
                .name(name)
                .userId(userId)
                .build();
        return projectRepository.update(project1);
    }

    @Override
    public Optional<Project> findByIndex(final int index) throws ProjectNotFoundException {
        return projectRepository.findByIndex(index);
    }

    @Override
    public Optional<List<Project>> findByName(final String name) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return Optional.empty();
        return projectRepository.findByName(name);
    }

    @Override
    public Optional<Project> findById(final Long id) throws ProjectNotFoundException {
        if (id == null) return Optional.empty();
        return projectRepository.findById(id);
    }

    @Override
    public Optional<Project> removeByIndex(final Integer index) throws ProjectNotFoundException {
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Optional<Project> removeById(final Long id) throws ProjectNotFoundException {
        if (id == null) return Optional.empty();
        return projectRepository.removeById(id);
    }

    @Override
    public Optional<List<Project>> removeByName(final String name) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return Optional.empty();
        return projectRepository.removeByName(name);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Optional<List<Project>> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Optional<List<Project>> findAllByUserId(Long userId) {
        if (userId == null) return Optional.empty();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        return projectRepository.saveJSON(fileName);
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        return projectRepository.saveXML(fileName);
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        return projectRepository.uploadJSON(fileName, Project.class);
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        return projectRepository.uploadXML(fileName, Project.class);
    }
}
