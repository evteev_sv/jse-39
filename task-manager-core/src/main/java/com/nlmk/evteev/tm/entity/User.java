package com.nlmk.evteev.tm.entity;

import com.nlmk.evteev.tm.enumerated.Role;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User extends AbstractEntity {

    private Long id = System.nanoTime();
    private String name = "";
    private String password;
    private String firstName = "";
    private String lastName = "";
    private Role role = Role.USER;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(String name, String password, String firstName, String lastName, Role role) {
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public User(Long id, String name, String password, String firstName, String lastName, Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

}
