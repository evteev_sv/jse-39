package com.nlmk.evteev.tm.observer.listener;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.enumerated.Action;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.observer.listener.interfaces.Listener;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import com.nlmk.evteev.tm.service.UserService;
import com.nlmk.evteev.tm.util.RequestParser;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

@Log4j2
public class ListenerProjectImpl implements Listener {

    private final ProjectService projectService;
    private final UserService userService;
    private final ProjectTaskService projectTaskService;

    public ListenerProjectImpl() {
        this.projectService = ProjectService.getInstance();
        this.userService = UserService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
    }

    @Override
    public int update(String param) throws ProjectNotFoundException, IOException {
        Action action = RequestParser.getAction(param);
        List<String> parameters = RequestParser.getParamsList(RequestParser.getParams(param));
        switch (action.getAction()) {
            case PROJECT_CREATE:
                return create(parameters);
            case PROJECT_CLEAR:
                return clear();
            case PROJECT_LIST:
                return list();
            case PROJECT_VIEW_BY_ID:
                return viewById(parameters);
            case PROJECT_REMOVE_BY_NAME:
                return removeByName(parameters);
            case PROJECT_UPDATE_BY_INDEX:
                return updateByIndex(parameters);
            case PROJECTS_TO_FILE_XML:
                return saveXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_TO_FILE_JSON:
                return saveJSON(PROJECTS_FILE_NAME_JSON);
            case PROJECTS_FROM_FILE_XML:
                return uploadFromXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_FROM_FILE_JSON:
                return uploadFromJSON(PROJECTS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }

    @Override
    public int create(List<String> parameters) {
        System.out.println("[CREATE PROJECT]");
        String name = Listener.getNameFromParameters(parameters);
        String description = "";
        if (name.equals("")) {
            return -1;
        }
        if (Listener.checkProjectName(name)) {
            return -1;
        }
        if (parameters.size() > 1) {
            description = parameters.get(1);
        }
        if (userService.getCurrentUser() == null) projectService.create(name, description);
        else projectService.create(name, description, userService.getCurrentUser().getId());
        return 0;
    }


    @Override
    public int updateByIndex(List<String> parameters) throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        String name, description;
        int index;
        if (parameters.size() > 1) {
            index = Listener.inputIndexCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        final Long userId = userService.getCurrentUser().getId();
        final var project = projectService.findByIndex(index);
        if (project.isEmpty()) {
            System.out.println("[FAIL]");
            log.info("Project was not updated");
            return -1;
        }
        name = parameters.get(1);
        if (Listener.checkProjectName(name)) {
            log.info("Project was not updated");
            System.out.println("[FAIL]");
            return -1;
        }
        if (parameters.size() > 1) {
            description = parameters.get(2);
            projectService.update(project.get().getId(), name, description, userId);
        } else {
            projectService.update(project.get().getId(), name, userId);
        }
        log.info("Project was updated");
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int clear() throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT]");
        if (userService.getCurrentUser() == null) {
            projectService.clear();
            projectTaskService.clear();
        } else {
            for (Project project : projectService
                    .findAllByUserId(userService.getCurrentUser().getId())
                    .orElse(new ArrayList<>())) {
                projectService.removeById(project.getId());
            }
        }
        log.info("Projects were deleted.");
        System.out.println("Projects were deleted.");
        return 0;
    }

    @Override
    public int removeByName(List<String> parameters) throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY NAME]");
        String name = Listener.getNameFromParameters(parameters);
        if (name.equals("")) {
            return -1;
        }
        final var projects = projectService.removeByName(name);
        if (projects.isEmpty()) {
            System.out.println("[FAIL]");
            log.info("Project was not removed.");
            return -1;
        }
        projects.get().forEach(project -> {
            final var tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks.orElse(new ArrayList<>())) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
        });
        log.info("Project was removed.");
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public void view(final Long id) throws ProjectNotFoundException {
        final var project = projectService.findById(id);
        if (project.isEmpty()) {
            return;
        }
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.get().getId());
        System.out.println("NAME: " + project.get().getName());
        System.out.println("DESCRIPTION: " + project.get().getDescription());
    }

    @Override
    public int viewById(List<String> parameters) throws ProjectNotFoundException {
        long id;
        if (parameters.size() > 0) {
            id = Listener.inputIdCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        view(id);
        return 0;
    }

    @Override
    public int list() {
        Optional<List<Project>> projectList;
        if (userService.getCurrentUser() == null) {
            projectList = projectService.findAll();
        } else {
            projectList = projectService.findAllByUserId(userService.getCurrentUser().getId());
        }
        return Listener.viewMultiple(projectList
                .orElse(Collections.emptyList())
                .toArray(AbstractEntity[]::new));

    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        projectService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        projectService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        projectService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        projectService.uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }
}
