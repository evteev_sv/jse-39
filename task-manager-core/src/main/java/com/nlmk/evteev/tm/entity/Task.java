package com.nlmk.evteev.tm.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Task extends AbstractEntity {

    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    private Long projectId;
    private Long userId;
    private LocalDateTime deadline;

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
        this.deadline = LocalDateTime.now().plusMinutes(480L);
    }

    public Task(String name, String description, Long userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.deadline = LocalDateTime.now().plusMinutes(480L);
    }

    public Task(String name, String description, Long userId, Long minutes) {
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.deadline = LocalDateTime.now().plusMinutes(minutes);
    }

    public Task(String name, String description, Long projectId, Long userId, LocalDateTime deadline) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
        this.deadline = deadline;
    }

    public Task(Long id, String name, String description, Long projectId, Long userId, LocalDateTime deadline) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
        this.deadline = deadline;
    }

}
