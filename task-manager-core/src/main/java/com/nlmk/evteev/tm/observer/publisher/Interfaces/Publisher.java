package com.nlmk.evteev.tm.observer.publisher.Interfaces;

import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.observer.listener.interfaces.Listener;

import java.io.IOException;
import java.util.Scanner;

public interface Publisher {

    void addListener(Listener listener);

    void deleteListener(Listener listener);

    void notify(String command) throws NotFoundException, IOException;

    void start(Scanner scanner) throws NotFoundException, IOException;

    boolean checkCommand(String command);

}
