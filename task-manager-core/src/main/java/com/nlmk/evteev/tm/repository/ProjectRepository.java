package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.exception.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.interfaces.ITaskProjectRepository;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@NoArgsConstructor
public class ProjectRepository extends AbstractRepository<Project> implements ITaskProjectRepository<Project> {

    private static ProjectRepository instance = null;
    private final UserRepository userRepository = UserRepository.getInstance();

    public static ProjectRepository getInstance() {
        if (instance == null) {
            synchronized (ProjectRepository.class) {
                if (instance == null) {
                    instance = new ProjectRepository();
                }
            }
        }
        return instance;
    }

    @Override
    public String getName(Project project) {
        if (project == null) return null;
        return project.getName();
    }

    @Override
    public Long getUserId(Optional<Project> project) {
        return project.map(Project::getUserId).orElse(null);
    }

    @Override
    public Optional<Project> update(final Project project1) throws ProjectNotFoundException {
        Project project = findById(project1.getId()).orElse(null);
        if (project == null) return Optional.empty();
        String oldName = project.getName();
        var projectListOld = Optional.ofNullable(itemsA
                .get(oldName))
                .orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + oldName + "."));
        if (projectListOld.isEmpty()) return Optional.empty();
        project.setName(project1.getName());
        if (project1.getDescription() != null) {
            project.setDescription(project1.getDescription());
        }
        if (!oldName.equals(project1.getName())) {
            projectListOld.remove(project);
            if (projectListOld.size() == 0) itemsA.remove(oldName);
            addToMap(project);
        }
        return Optional.of(project);
    }

    @Override
    public Optional<Project> findByIndex(final int index) throws ProjectNotFoundException {
        Optional<List<Project>> result;
        User user = userRepository.currentUser;
        if (user == null) {
            result = findAll();
        } else {
            result = findAllByUserId(user.getId());
        }
        result.orElseThrow(() -> new ProjectNotFoundException("Project is not found by index " + index + "."));
        if (index < 0 || index > result.get().size() - 1) {
            throw new ProjectNotFoundException("Wrong index.");
        }
        return Optional.ofNullable(result.get().get(index));
    }

    @Override
    public Optional<List<Project>> findByName(final String name) throws ProjectNotFoundException {
        if (!itemsA.containsKey(name)) {
            throw new ProjectNotFoundException("Project are not found by name " + name + ".");
        }
        Optional<List<Project>> result = Optional.of(new ArrayList<>());
        User user = userRepository.currentUser;
        if (user == null) {
            result = Optional.ofNullable(itemsA.get(name));
        } else {
            itemsA.get(name).stream()
                    .filter(project -> project.getUserId().equals(user.getId()))
                    .forEach(result.get()::add);
        }
        result.orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + name + "."));
        return result;
    }

    @Override
    public Optional<Project> findById(final Long id) throws ProjectNotFoundException {
        Optional<List<Project>> currentListTask;
        User user = userRepository.currentUser;
        if (user == null) {
            currentListTask = findAll();
        } else {
            currentListTask = findAllByUserId(user.getId());
        }
        currentListTask.orElseThrow(() -> new ProjectNotFoundException("Project is not found by " + id + "."));
        for (final Project project : currentListTask.get()) {
            if (project.getId().equals(id)) return Optional.of(project);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Project> removeByIndex(final int index) throws ProjectNotFoundException {
        Project project = findByIndex(index).orElse(null);
        if (project == null) return Optional.empty();
        items.remove(project);
        Optional.ofNullable(itemsA.get(project.getName())).orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + project.getName() + "."));
        List<Project> projectFromName = Optional.ofNullable(itemsA
                .get(project.getName()))
                .orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + project.getName() + "."));
        projectFromName.remove(project);
        return Optional.of(project);
    }

    @Override
    public Optional<Project> removeById(final Long id) throws ProjectNotFoundException {
        Project project = findById(id).orElse(null);
        if (project == null) return Optional.empty();
        items.remove(project);
        List<Project> projectFromName = Optional.ofNullable(itemsA
                .get(project.getName()))
                .orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + project.getName() + "."));
        projectFromName.remove(project);
        return Optional.of(project);
    }

    @Override
    public Optional<List<Project>> removeByName(final String name) throws ProjectNotFoundException {
        var projectList = findByName(name);
        if (projectList.isEmpty()) return Optional.empty();
        List<Project> projectListFromName = Optional.ofNullable(itemsA
                .get(name))
                .orElseThrow(() -> new ProjectNotFoundException("Projects are not found by name " + name + "."));
        items.removeAll(projectList.get());
        projectListFromName.removeAll(projectList.get());
        return projectList;
    }
}
