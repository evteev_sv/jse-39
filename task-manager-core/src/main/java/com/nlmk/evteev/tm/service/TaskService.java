package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exception.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.interfaces.ITaskIService;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class TaskService implements ITaskIService {

    private static TaskService instance = null;
    private final TaskRepository taskRepository = TaskRepository.getInstance();
    private final UserService userService = UserService.getInstance();


    public static TaskService getInstance() {
        if (instance == null) {
            synchronized (TaskService.class) {
                if (instance == null) {
                    instance = new TaskService();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<Task> create(final String name) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .id(System.nanoTime())
                .name(name)
                .build();
        return taskRepository.create(task1);
    }

    @Override
    public Optional<Task> create(final String name, final String description) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .id(System.nanoTime())
                .name(name)
                .description(description)
                .build();
        return taskRepository.create(task1);
    }

    @Override
    public Optional<Task> create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .id(System.nanoTime())
                .name(name)
                .description(description)
                .userId(userId)
                .build();
        return taskRepository.create(task1);
    }

    @Override
    public Optional<Task> create(final String name, final String description, final Long userId, final LocalDateTime time) {
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .name(name)
                .description(description)
                .userId(userId)
                .deadline(time)
                .build();
        return taskRepository.create(task1);
    }

    @Override
    public Optional<Task> update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        if (id == null) return Optional.empty();
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .id(id)
                .name(name)
                .description(description)
                .userId(userId)
                .build();
        return taskRepository.update(task1);
    }

    @Override
    public Optional<Task> update(final Long id, final String name, final Long userId) throws TaskNotFoundException {
        if (id == null) return Optional.empty();
        if (name == null || name.isEmpty()) return Optional.empty();
        Task task1 = Task.builder()
                .id(id)
                .name(name)
                .userId(userId)
                .build();
        return taskRepository.update(task1);
    }

    @Override
    public Optional<Task> findByIndex(final int index) throws TaskNotFoundException {
        return taskRepository.findByIndex(index);
    }

    @Override
    public Optional<List<Task>> findByName(final String name) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return Optional.empty();
        return taskRepository.findByName(name);
    }

    @Override
    public Optional<Task> findById(final Long id) throws TaskNotFoundException {
        if (id == null) return Optional.empty();
        return taskRepository.findById(id);
    }

    @Override
    public Optional<Task> removeByIndex(final Integer index) throws TaskNotFoundException {
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Optional<Task> removeById(final Long id) throws TaskNotFoundException {
        if (id == null) return Optional.empty();
        return taskRepository.removeById(id);
    }

    @Override
    public Optional<List<Task>> removeByName(final String name) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return Optional.empty();
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Optional<List<Task>> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Optional<List<Task>> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Optional.empty();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Optional<Task> findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return Optional.empty();
        if (id == null) return Optional.empty();
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    @Override
    public Optional<List<Task>> findAllByUserId(final Long userId) {
        if (userId == null) return Optional.empty();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        return taskRepository.saveJSON(fileName);
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        return taskRepository.saveXML(fileName);
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        return taskRepository.uploadJSON(fileName, Task.class);
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        return taskRepository.uploadXML(fileName, Task.class);
    }

}
