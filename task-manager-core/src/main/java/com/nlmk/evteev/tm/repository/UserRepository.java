package com.nlmk.evteev.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import com.nlmk.evteev.tm.repository.interfaces.IUserRepository;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.*;

@NoArgsConstructor
public class UserRepository implements IUserRepository<User> {

    private static UserRepository instance = null;
    public User currentUser;
    public Deque<String> history = new ArrayDeque<>();
    public int historyLimit = 10;
    private List<User> users = new ArrayList<>();

    public static UserRepository getInstance() {
        if (instance == null) {
            synchronized (UserRepository.class) {
                if (instance == null) {
                    instance = new UserRepository();
                }
            }
        }
        return instance;
    }

    @Override
    public Optional<User> create(final User user1) {
        User user = findByName(user1.getName()).orElse(null);
        if (user == null) {
            users.add(user1);
            return Optional.of(user1);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> update(final User user1) {
        User user;
        if (!user1.getName().equals("")) {
            user = findByName(user1.getName()).orElse(null);
        } else {
            user = findById(user1.getId()).orElse(null);
        }
        if (user == null) return Optional.empty();
        user.setPassword(user1.getPassword());
        user.setFirstName(user1.getFirstName());
        user.setLastName(user1.getLastName());
        return Optional.of(user);
    }

    @Override
    public Optional<User> updatePassword(final User user1) {
        User user;
        if (!user1.getName().equals("")) {
            user = findByName(user1.getName()).orElse(null);
        } else {
            user = findById(user1.getId()).orElse(null);
        }
        if (user == null) return Optional.empty();
        user.setPassword(user1.getPassword());
        return Optional.of(user);
    }

    @Override
    public Optional<User> updateRole(final User user1) {
        User user;
        if (!user1.getName().equals("")) {
            user = findByName(user1.getName()).orElse(null);
        } else {
            user = findById(user1.getId()).orElse(null);
        }
        if (user == null) return Optional.empty();
        user.setRole(user1.getRole());
        return Optional.of(user);
    }

    @Override
    public Optional<User> findByName(final String login) {
        for (final User user : users) {
            if (user.getName().equals(login)) return Optional.of(user);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return Optional.of(user);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findByIndex(int index) throws UserNotFoundException {
        Optional<List<User>> result;
        result = findAll();
        result.orElseThrow(() -> new UserNotFoundException("User is not found by index " + index + "."));
        if (index < 0 || index > result.get().size() - 1) throw new UserNotFoundException("Wrong index.");
        return Optional.ofNullable(result.get().get(index));
    }

    @Override
    public Optional<User> removeByName(final String login) {
        User user = findByName(login).orElse(null);
        if (user == null) return Optional.empty();
        users.remove(user);
        return Optional.of(user);
    }

    @Override
    public Optional<User> removeById(final Long id) {
        User user = findById(id).orElse(null);
        if (user == null) return Optional.empty();
        users.remove(user);
        return Optional.of(user);
    }

    @Override
    public Optional<User> removeByIndex(final int index) throws UserNotFoundException {
        User user = findByIndex(index).orElse(null);
        if (user == null) return Optional.empty();
        users.remove(user);
        return Optional.of(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public Optional<List<User>> findAll() {
        return Optional.of(users);
    }

    @Override
    public int saveJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, users);
        return 0;
    }

    @Override
    public int saveXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File(fileName), users);
        return 0;
    }

    @Override
    public int uploadJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        users.clear();
        users = objectMapper.readValue(file, new TypeReference<>() {
        });
        return 0;
    }

    @Override
    public int uploadXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, User.class);
        clear();
        users = xmlMapper.readValue(new File(fileName), type);
        return 0;
    }

    @Override
    public int addCommandToHistory(String command) {
        if (command == null) return -1;
        history.add(command);
        if (history.size() > historyLimit) {
            history.remove();
            return 1;
        }
        return 0;
    }

}
