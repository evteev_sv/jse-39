package com.nlmk.evteev.tm.observer.listener;

import com.nlmk.evteev.tm.entity.AbstractEntity;
import com.nlmk.evteev.tm.enumerated.Action;
import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.exception.UserNotFoundException;
import com.nlmk.evteev.tm.observer.listener.interfaces.Listener;
import com.nlmk.evteev.tm.observer.listener.interfaces.ListenerUser;
import com.nlmk.evteev.tm.service.UserService;
import com.nlmk.evteev.tm.util.RequestParser;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

@Log4j2
public class ListenerUserImpl implements ListenerUser {

    private final UserService userService = UserService.getInstance();

    @Override
    public int update(String param) throws IOException, NotFoundException {
        Action action = RequestParser.getAction(param);
        List<String> parameters = RequestParser.getParamsList(RequestParser.getParams(param));
        switch (action.getAction()) {
            case HISTORY:
                return displayHistory();
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();
            case LOG_ON:
                return registry(parameters);
            case LOG_OFF:
                return logOff();
            case USER_INFO:
                return displayUserInfo();
            case USER_UPDATE_PASSWORD:
                return updatePassword(parameters);
            case USER_PASSWORD_UPDATE_BY_LOGIN:
                return updatePasswordByLogin(parameters);
            case USER_CREATE:
                return create(parameters);
            case USER_CLEAR:
                return clear();
            case USER_LIST:
                return list();
            case USER_VIEW_BY_ID:
                return viewById(parameters);
            case USER_UPDATE_BY_LOGIN:
                return updateByIndex(parameters);
            case USER_REMOVE_BY_LOGIN:
                return removeByName(parameters);
            case USERS_TO_FILE_XML:
                return saveXML(USERS_FILE_NAME_XML);
            case USERS_TO_FILE_JSON:
                return saveJSON(USERS_FILE_NAME_JSON);
            case USERS_FROM_FILE_XML:
                return uploadFromXML(USERS_FILE_NAME_XML);
            case USERS_FROM_FILE_JSON:
                return uploadFromJSON(USERS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }

    @Override
    public int displayHistory() {
        if (userService.getHistory() == null || userService.getHistory().isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return -1;
        }
        int index = 1;
        for (final String command : userService.getHistory()) {
            System.out.println(index + ". " + command);
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int displayExit() {
        System.out.println("Terminate console application...");
        return 0;
    }

    @Override
    public int displayHelp() {
        System.out.println("log-on - Log on.");
        System.out.println("log-off - Log off.");
        System.out.println("user-info - User information.");
        System.out.println("user-update - Update user information.");
        System.out.println("user-update-password - Update current user password.");
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println("history - History of commands.");
        System.out.println();
        System.out.println("project-create - Create project.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println();
        System.out.println("task-create - Create task.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        System.out.println("user-create - Create user.");
        System.out.println("user-clear - Clear list of users.");
        System.out.println("user-list - Display list of users.");
        System.out.println("user-view-by-login - Display user.");
        System.out.println("user-remove-by-login - Remove user bu login.");
        System.out.println("user-update-by-login - Update user by login.");
        return 0;
    }

    @Override
    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    @Override
    public int displayAbout() {
        System.out.println("Kondratenko Iulia");
        System.out.println("kondratenko_yg@nlmk.com");
        return 0;
    }

    @Override
    public int create(List<String> parameters) {
        String login, password, firstName, lastName = "";
        if (parameters.size() > 2) {
            login = parameters.get(0);
            password = parameters.get(1);
            firstName = parameters.get(2);
        } else {
            return -1;
        }
        if (parameters.size() > 3) {
            lastName = parameters.get(3);
        }
        if (userService.create(login, password, firstName, lastName, Role.USER).isPresent()) {
            log.info("User is created.");
        } else {
            log.info("Choose different login.");
            return -1;
        }
        return 0;
    }

    @Override
    public int updateByIndex(List<String> parameters) throws UserNotFoundException {
        System.out.println("[UPDATE USER]");
        String password, firstName, lastName = "";
        int index;
        if (parameters.size() > 2) {
            index = Listener.inputIndexCheckFormat(parameters.get(0));
            password = parameters.get(1);
            firstName = parameters.get(2);
        } else {
            return -1;
        }
        if (parameters.size() > 3) {
            lastName = parameters.get(3);
        }
        final var user = userService.findByIndex(index);
        if (user.isEmpty()) {
            log.info("User is not found.");
            return -1;
        } else {
            userService.update(user.get().getName(), password, firstName, lastName);
            log.info("User is updated.");
        }
        return 0;
    }

    public int updatePasswordByLogin(List<String> parameters) {
        System.out.println("[UPDATE USER]");
        String login, password;
        if (parameters.size() > 1) {
            login = parameters.get(0);
            password = parameters.get(1);
        } else {
            return -1;
        }
        final var user = userService.findByName(login);
        if (user.isEmpty()) {
            log.info("User is not found.");
            return -1;
        } else {
            userService.updatePasswordByLogin(login, password);
            log.info("Password is updated.");
        }
        return 0;
    }

    @Override
    public int viewById(List<String> parameters) {
        long id;
        if (parameters.size() > 1) {
            id = Listener.inputIdCheckFormat(parameters.get(0));
        } else {
            return -1;
        }
        view(id);
        return 0;
    }

    @Override
    public int clear() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        log.info("User was deleted.");
        return 0;
    }

    @Override
    public int removeByName(List<String> parameters) {
        System.out.println("[CLEAR USER BY LOGIN]");
        String name = Listener.getNameFromParameters(parameters);
        if (name.equals("")) {
            return -1;
        }
        final var user = userService.removeByName(name);
        if (user.isEmpty()) {
            log.info("User was not deleted by login.");
            return -1;
        }
        log.info("User was deleted by login.");
        return 0;
    }

    @Override
    public void view(final Long userId) {
        final var user = userService.findById(userId);
        if (user.isEmpty()) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.get().getId());
        System.out.println("LOGIN: " + user.get().getName());
        System.out.println("PASSWORD HASH: " + user.get().getPassword());
        System.out.println("FIRST NAME: " + user.get().getFirstName());
        System.out.println("LAST NAME: " + user.get().getLastName());
        System.out.println("ROLE: " + user.get().getRole().getDisplayName());
        System.out.println("[OK]");
    }

    @Override
    public int list() {
        System.out.println("[LIST USER]");
        return Listener.viewMultiple(userService
                .findAll()
                .orElse(Collections.emptyList())
                .toArray(AbstractEntity[]::new));
    }

    @Override
    public int displayUserInfo() {
        view(userService.getCurrentUser().getId());
        return 0;
    }

    @Override
    public int registry(List<String> parameters) {
        String login, password;
        if (parameters.size() > 1) {
            login = parameters.get(0);
            password = parameters.get(1);
        } else {
            return -1;
        }
        final var user = userService.findByName(login);
        if (user.isEmpty()) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM.");
            return -1;
        }
        if (userService.checkPassword(user, password)) {
            System.out.println("WELCOME, " + user.get().getName());
            userService.setCurrentUser(user.get());
        } else {
            System.out.println("FAIL");
            return -1;
        }
        return 0;
    }

    @Override
    public int logOff() {
        userService.setCurrentUser(null);
        System.out.println("LOG OFF");
        return 0;
    }

    @Override
    public int updatePassword(List<String> parameters) {
        System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
        if (userService.getCurrentUser() == null) {
            System.out.println("[FAIL]");
            return -1;
        } else {
            String newPassword, oldPassword;
            if (parameters.size() > 1) {
                oldPassword = parameters.get(0);
                newPassword = parameters.get(1);
            } else {
                return -1;
            }
            if (userService.checkPassword(Optional.of(userService.getCurrentUser()), oldPassword)) {
                System.out.println("[PLEASE, ENTER NEW PASSWORD:]");
                userService.updatePasswordByLogin(userService.getCurrentUser().getName(), newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[WRONG PASSWORD]");
                return -1;
            }
        }
        return 0;
    }

    @Override
    public int saveJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        userService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        userService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromJSON(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        userService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    @Override
    public int uploadFromXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        userService.uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }
}
