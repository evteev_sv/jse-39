package com.nlmk.evteev.tm.observer.publisher;

import com.nlmk.evteev.tm.enumerated.Action;
import com.nlmk.evteev.tm.enumerated.Role;
import com.nlmk.evteev.tm.exception.NotFoundException;
import com.nlmk.evteev.tm.observer.listener.interfaces.Listener;
import com.nlmk.evteev.tm.observer.publisher.Interfaces.Publisher;
import com.nlmk.evteev.tm.service.UserService;
import com.nlmk.evteev.tm.util.RequestParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.ALL_ACTIONS;
import static com.nlmk.evteev.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher {

    private final UserService userService;
    private List<Listener> listeners;

    public PublisherImpl(UserService userService) {
        this.listeners = new ArrayList<>();
        this.userService = userService;
    }

    @Override
    public void start(Scanner scanner) throws NotFoundException, IOException {
        var command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            if (!checkCommand(command)) {
                continue;
            }
            userService.addCommandToHistory(command);
            notify(command);
            System.out.println();
        }
    }

    @Override
    public boolean checkCommand(String command) {
        if (command == null || command.isEmpty()) return false;
        Action action = RequestParser.getAction(command);
        if (!Arrays.asList(ALL_ACTIONS).contains(action.getAction())) {
            if (!Arrays.asList(Role.ADMIN.getActions()).contains(action.getAction())) {
                System.out.println("[FAIL]");
                return false;
            }
            if (userService.getCurrentUser() == null) {
                System.out.println("[PLEASE, LOG ON OR REGISTRY.]");
                return false;
            }
            if (!Arrays.asList(userService.getCurrentUser().getRole().getActions()).contains(action.getAction())) {
                System.out.println("[NOT ALLOWED.]");
                return false;
            }
        }
        return true;
    }

    @Override
    public void addListener(Listener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notify(String command) throws NotFoundException, IOException {
        for (Listener listener : listeners) {
            listener.update(command);
        }
    }

}
